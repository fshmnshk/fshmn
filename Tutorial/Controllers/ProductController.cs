﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tutorial.Models;

namespace Tutorial.Controllers
{
    public class ProductController : Controller
    {
        private DatabaseContext db = new DatabaseContext();
        // GET: Product
        public ActionResult Index(int CategoryID)
        {
            List<Product> products = db.Products.Where(x => x.Category.ID == CategoryID).ToList();
            ViewBag.Category = db.Categories.Where(x => x.ID == CategoryID).FirstOrDefault();

            return View(products);
        }

        public ActionResult Create(int CategoryID)
        {
            List<Category> categories = db.Categories.ToList();
            ViewBag.Category = db.Categories.Where(x => x.ID == CategoryID).FirstOrDefault();

            return View(categories);
        }

        [HttpPost]
        public ActionResult Create(string emri, string pershkrimi, int CategoryID)
        {
            Product product = new Product();
            product.Emri = emri;
            product.Pershkrimi = pershkrimi;
            product.Category = db.Categories.Where(x => x.ID == CategoryID).FirstOrDefault();

            db.Products.Add(product);
            db.SaveChanges();

            return RedirectToAction("Index", new { CategoryID = CategoryID });
        }
    }
}