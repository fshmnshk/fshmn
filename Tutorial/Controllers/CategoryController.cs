﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tutorial.Models;

namespace Tutorial.Controllers
{
    public class CategoryController : Controller
    {
        private DatabaseContext db = new DatabaseContext();
        // GET: Category
        public ActionResult Index()
        {            
            List<Category> categories = new List<Category>();
            categories = getCategoriesFromDb();

            return View(categories);
        }

        public ActionResult Detail(int ID)
        {
            Category category = db.Categories.Where(x => x.ID == ID).FirstOrDefault();

            return View(category);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(string emri, string pershkrimi)
        {
            Category category = new Category();
            category.Emri = emri;
            category.Pershkrimi = pershkrimi;

            db.Categories.Add(category);
            db.SaveChanges();

            return RedirectToAction("Index");
        }


        public ActionResult Edit(int ID)
        {
            Category category = db.Categories.Where(x => x.ID == ID).FirstOrDefault();

            return View(category);
        }

        [HttpPost]
        public ActionResult Edit(int ID, string emri, string pershkrimi)
        {
            Category category = db.Categories.Where(x => x.ID == ID).FirstOrDefault();
            category.Emri = emri;
            category.Pershkrimi = pershkrimi;

            db.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult Delete(int ID)
        {
            Category category = db.Categories.Where(x => x.ID == ID).FirstOrDefault();

            db.Categories.Remove(category);
            db.SaveChanges();

            return RedirectToAction("Index");
        }
        public List<Category> getCategories()
        {
            List<Category> categories = new List<Category>();
            Category pc = new Category();
            pc.ID = 1;
            pc.Emri = "PC";

            Category laptop = new Category();
            laptop.ID = 2;
            laptop.Emri = "Lap Top";

            Category printer = new Category();
            printer.ID = 3;
            printer.Emri = "Printer";

            categories.Add(pc);
            categories.Add(laptop);
            categories.Add(printer);

            return categories;
        }

        public List<Category> getCategoriesFromDb()
        {
            List<Category> categories = db.Categories.ToList();

            return categories;
        }
    }
}