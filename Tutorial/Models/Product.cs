﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tutorial.Models
{
    public class Product
    {
        public int ID { get; set; }
        public string Emri { get; set; }
        public string Pershkrimi { get; set; }
        public double Price { get; set; }

        public Category Category { get; set; }
    }
}